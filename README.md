# Phonebas

This application is a simple voice and SMS text service provided by [Twilio].

## Setup

Create a virtual host for Apache, something like the following:

    <VirtualHost *:80>
        ServerName phonebas.local
        ServerAlias www.phonebas.local
        
        DocumentRoot "/path/to/phonebas/public"
        
        <Directory "/path/to/phonebas/public">
        
            Options FollowSymlinks Indexes
            AllowOverride all
            Order allow,deny
            Allo from all
        </Directory>
        
        SetEnv TWILIO_ACCOUNT_SID=YOURTW1LI0KACC0UN7S1D 
        SetEnv TWILIO_AUTH_KEY=YOURTW1LI0AU7HK3Y 
        SetEnv TWILIO_PHONENR=+123467890 
        SetEnv TWILIO_FWDNR=+9876543210
        
        ErrorLog "/var/log/apache2/phonebas-error_log"
        AccessLog "/var/log/apache2/phonebas-access_log" common
    </VirtualHost>

## Listing the send SMS's

Point your browser to http://www.phonebas.local/receive-sms.php to get the listing of text messages.

[Twilio]: https://twilio.com