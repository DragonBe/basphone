<?php
if (file_exists('local.config.php')) {
    require_once 'local.config.php';
}
$config = array (
    'AccountSid' => getenv('TWILIO_ACCOUNT_SID'),
    'AuthToken' => getenv('TWILIO_AUTH_KEY'),
    'PhoneNumber' => getenv('TWILIO_PHONENR'),
    'ForwardNumber' => getenv('TWILIO_FWDNR'),
);