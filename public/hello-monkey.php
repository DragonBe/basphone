<?php
require_once __DIR__ . '/../config.php';
header('Content-Type: text/xml; Charset=UTF-8');
echo '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL;
?>
<Response>
    <Dial><?php echo $config['ForwardNumber'] ?></Dial>
    <Say>The call failed or the remote party hung up. Goodbye.</Say>
</Response>
