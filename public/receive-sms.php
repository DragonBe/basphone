<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config.php';

$errors = array ();
$messages = array ();

$twClient = new Services_Twilio($config['AccountSid'], $config['AuthToken']);
try {
    $messages = $twClient->account->messages->getIterator(0, 10, array ('DateSent' => '>=2016-01-01'));
} catch (\Services_Twilio_RestException $e) {
    $errors[] = $e->getMessage();
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
        <title>Incoming Messages on <?php echo $config['PhoneNumber'] ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    </head>

    <body>
        <div class="container">
            <h1>Incoming Messages on <?php echo $config['PhoneNumber'] ?></h1>
            <?php if (array () !== $messages): ?>
                <table class="table table-striped">
                    <tr>
                        <th>Date</th>
                        <th>From</th>
                        <th>To</th>
                        <th>Message</th>
                        <th>Price</th>
                        <th>Currency</th>
                    </tr>
                    <?php foreach ($messages as $message): ?>
                        <?php
                            /** @var \Services_Twilio_Rest_Message $message */
                            $date = new DateTime($message->date_created);
                            if ('2016-01-01' > $date->format('Y-m-d')) continue;
                        ?>
                        <tr>
                            <td><?php echo $message->date_created ?></td>
                            <td><a href="sms-detail.php?sid=<?php echo $message->sid ?>"><?php echo $message->from ?></a></td>
                            <td><?php echo $message->to ?></td>
                            <td><?php echo (200 < strlen($message->body) ? substr($message->body, 0, 200) . '...' : $message->body) ?></td>
                            <td><?php echo $message->price ?></td>
                            <td><?php echo $message->price_unit ?></td>
                        </tr>
                    <?php endforeach ?>
                </table>
            <?php endif ?>
            <?php foreach ($errors as $error): ?>
                <div class="error"><?php echo $error ?></div>
            <?php endforeach ?>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
