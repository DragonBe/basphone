<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/../config.php';

$errors = array ();
$message = null;
$sid = $_GET['sid'];

$twClient = new Services_Twilio($config['AccountSid'], $config['AuthToken']);
try {
    $message = $twClient->account->messages->get($sid);
} catch (\Services_Twilio_RestException $e) {
    $errors[] = $e->getMessage();
}
?>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; Charset=UTF-8">
        <title>Incoming Messages on <?php echo $config['PhoneNumber'] ?></title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css">
    </head>

    <body>
        <div class="container">
            <h1>Incoming Messages on <?php echo $config['PhoneNumber'] ?></h1>
            <div>
                <strong>Date:</strong> <?php echo $message->date_created ?><br>
                <strong>SID:</strong> <?php echo $message->sid ?><br>
                <strong>Direction:</strong> <?php echo $message->direction ?><br>
                <strong>From:</strong> <?php echo $message->from ?><br>
                <strong>To:</strong> <?php echo $message->to ?><br>
                <strong>Message:</strong><br><?php echo $message->body ?><br><br>
                <a href="receive-sms.php" title="Back to list" class="btn btn-default">Back to list</a>
            </div>
            <?php foreach ($errors as $error): ?>
                <div class="error"><?php echo $error ?></div>
            <?php endforeach ?>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
